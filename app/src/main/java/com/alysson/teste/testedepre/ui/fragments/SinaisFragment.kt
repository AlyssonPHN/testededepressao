package com.alysson.teste.testedepre.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import com.alysson.teste.testedepre.R
import com.alysson.teste.testedepre.ui.activitys.ActEdicao
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"



/**
 * A simple [Fragment] subclass.
 * Use the [SinaisFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SinaisFragment : Fragment() {
    private var param1: Int? = null
    private lateinit var card1: CardView
    private lateinit var card2: CardView
    private lateinit var card3: CardView
    private lateinit var card4: CardView

    private lateinit var img1: ImageView
    private lateinit var img2: ImageView
    private lateinit var img3: ImageView
    private lateinit var img4: ImageView

    private lateinit var txt1: TextView
    private lateinit var txt2: TextView
    private lateinit var txt3: TextView
    private lateinit var txt4: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getInt(ARG_PARAM1)
            var teste = "sdsds"
            var tess = "sdsds"
            var tass = "sdsds"
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sinais, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        card1 = view.findViewById(R.id.card1)
        card2 = view.findViewById(R.id.card2)
        card3 = view.findViewById(R.id.card3)
        card4 = view.findViewById(R.id.card4)

        img1 = view.findViewById(R.id.img1)
        img2 = view.findViewById(R.id.img2)
        img3 = view.findViewById(R.id.img3)
        img4 = view.findViewById(R.id.img4)

        txt1 = view.findViewById(R.id.txt1)
        txt2 = view.findViewById(R.id.txt2)
        txt3 = view.findViewById(R.id.txt3)
        txt4 = view.findViewById(R.id.txt4)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        preencherImagens()
        clickCardsView()
    }


    private fun preencherImagens(){
//        val myOptions = RequestOptions().fitCenter()
//                .override(200, 200)

        when(param1){
            0 -> {
                Glide.with(context!!).load(getImage("women"))
                        .centerCrop()
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(img1)
                Glide.with(context!!).load(getImage("mente_externo"))
                        .centerCrop()
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(img2)
                Glide.with(context!!).load(getImage("social_externo"))
                        .centerCrop()
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(img3)
                Glide.with(context!!).load(getImage("espiritual_externon"))
                        .centerCrop()
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(img4)
            }
            1 -> {
                Glide.with(context!!).load(getImage("corpo_interno"))
                        .centerCrop()
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(img1)
                Glide.with(context!!).load(getImage("mente_interno"))
                        .centerCrop()
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(img2)
                Glide.with(context!!).load(getImage("social"))
                        .centerCrop()
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(img3)
                Glide.with(context!!)
                        .asBitmap()
                        .centerCrop()
                        .load(getImage("espiritual"))
                        .into(img4)
            }
        }
    }

    private fun clickCardsView(){
        card1.setOnClickListener {
            mostrarCardCorrespondente(param1!!, ContentSinaisFragment.CORPO)
        }
        card2.setOnClickListener {
            mostrarCardCorrespondente(param1!!, ContentSinaisFragment.MENTE)
        }
        card3.setOnClickListener {
            mostrarCardCorrespondente(param1!!, ContentSinaisFragment.SOCIAL)
        }
        card4.setOnClickListener {
            mostrarCardCorrespondente(param1!!, ContentSinaisFragment.ESPIRITO)
        }
    }

    private fun mostrarCardCorrespondente(param1: Int,  opcao: Int){
        val fragment = ContentSinaisFragment.newInstance(param1, opcao)
        val i = Intent(context!!, ActEdicao::class.java)
        ActEdicao.fragment = fragment
        ActEdicao.tituloFragmento = getString(R.string.app_name)
        startActivity(i)
    }

    fun getImage(imageName: String?): Int {
        return resources.getIdentifier(imageName, "drawable", context!!.packageName)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: Int) =
                SinaisFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_PARAM1, param1)
                    }
                }
    }
}