package com.alysson.teste.testedepre.controle;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.widget.RadioButton;
import android.widget.Toast;

import com.alysson.teste.testedepre.R;
import com.alysson.teste.testedepre.ui.activitys.MainActivity;


public class MetodosemComuns {

    public static boolean verificarRadioButton(Context context, RadioButton radioBtn1, RadioButton radioBtn2,
                                            RadioButton radioBtn3, RadioButton radioBtn4){
        if (radioBtn1.isChecked()){
            MainActivity.setContador(1);
            return true;
        } else if (radioBtn2.isChecked()){
            MainActivity.setContador(2);
            return true;
        } else if (radioBtn3.isChecked()) {
            MainActivity.setContador(3);
            return true;
        } else if (radioBtn4.isChecked()) {
            MainActivity.setContador(4);
            return true;
        } else {
            Toast.makeText(context, "Escolha uma opção", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public static void chamarFragment(FragmentActivity context, Fragment fragment){
        FragmentManager fragmentManager = context.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
    }
}
