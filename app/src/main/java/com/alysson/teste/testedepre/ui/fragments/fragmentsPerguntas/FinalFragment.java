package com.alysson.teste.testedepre.ui.fragments.fragmentsPerguntas;


import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.alysson.teste.testedepre.DB.FeedReaderContract;
import com.alysson.teste.testedepre.DB.FeedReaderDbHelper;
import com.alysson.teste.testedepre.R;
import com.alysson.teste.testedepre.controle.MetodosemComuns;
import com.alysson.teste.testedepre.ui.activitys.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class FinalFragment extends Fragment implements View.OnClickListener {


    private TextView resposta, txtpontos;
    private Button btn_proximo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_final, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        resposta = (TextView) view.findViewById(R.id.resposta);
        txtpontos = (TextView) view.findViewById(R.id.pontos);
        btn_proximo = (Button) view.findViewById(R.id.btn_proximo);
        btn_proximo.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //Mostra a propaganda intersticial ao entrar na tela final
        MainActivity.interstitialAd.show();

        //Calcular Resposta
        mostrarResposta();
    }

    private void mostrarResposta() {
        if (MainActivity.getContador() <= 31) {
            resposta.setText(R.string.baixa);
        } else if (MainActivity.getContador() <= 43) {
            resposta.setText(R.string.media_baixa);
        } else if (MainActivity.getContador() <= 55) {
            resposta.setText(R.string.media);
        } else if (MainActivity.getContador() <= 67) {
            resposta.setText(R.string.media_alta);
        } else if (MainActivity.getContador() <= 80) {
            resposta.setText(R.string.alta);
        }
        txtpontos.setText(String.valueOf(MainActivity.getContador()));
        new SalvarTesteDepressao().execute();

    }

    private class SalvarTesteDepressao extends AsyncTask<Void, Void, Exception> {

        // Acessar banco
        FeedReaderDbHelper mDbHelper = FeedReaderDbHelper.getInstance(getContext());

        @Override
        protected Exception doInBackground(Void... params) {
            Date data = new Date();
            SimpleDateFormat formatador = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            // Gets the data repository in write mode
            SQLiteDatabase db;
            db = mDbHelper.getWritableDatabase();

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(FeedReaderContract.tabelaDepressao.COLUMN_PONTOS, MainActivity.getContador());
            values.put(FeedReaderContract.tabelaDepressao.COLUMN_DATA, formatador.format(data));

            try {
                //if (verificarseJaTemAChave(db, cupom.getChaveDeAcesso())) {
                db.insert(FeedReaderContract.tabelaDepressao.TABLE_NAME, null, values);
                //} else {
                //throw new Exception("Cupom já salvo!");
                //}
            } catch (Exception e) {
                return e;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Exception e) {
            super.onPostExecute(e);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btn_proximo) {
            //Mudar de fragment
            MainActivity.contador = 0;
            //Ir para o fragment inicial
            Fragment fragment = new HomeFragment();
            MetodosemComuns.chamarFragment(getActivity(), fragment);
        }

    }
}
