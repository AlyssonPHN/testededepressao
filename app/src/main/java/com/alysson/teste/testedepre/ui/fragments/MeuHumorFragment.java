package com.alysson.teste.testedepre.ui.fragments;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.alysson.teste.testedepre.DB.FeedReaderContract;
import com.alysson.teste.testedepre.DB.FeedReaderDbHelper;
import com.alysson.teste.testedepre.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;


public class MeuHumorFragment extends Fragment {

    private int posicao;

    public MeuHumorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MyAdapter adapter = new MyAdapter();
        final FeatureCoverFlow mCoverFlow = (FeatureCoverFlow) view.findViewById(R.id.coverflow);
        mCoverFlow.setAdapter(adapter);
        //mCoverFlow.setSelection(adapter.getCount()/2); //adapter.getCount()-1
        //carousel.setSlowDownCoefficient(1);
        mCoverFlow.setSpacing(0.5f);

        mCoverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //TODO CoverFlow item clicked
                Toast.makeText(getContext(), "Selecionado"+" ", Toast.LENGTH_SHORT).show();
                posicao = position+1;
                new SalvarHumor().execute();
            }
        });

        mCoverFlow.setOnScrollPositionListener(new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
                //Aqui vou deixar selecionado a imagem
                mCoverFlow.setSelection(position);
            }

            @Override
            public void onScrolling() {
                //TODO CoverFlow began scrolling

            }
        });
    }

    private class SalvarHumor extends AsyncTask<Void, Void, Exception>{

        // Acessar banco
        FeedReaderDbHelper mDbHelper = FeedReaderDbHelper.getInstance(getContext());


        @Override
        protected Exception doInBackground(Void... params) {
            Date data = new Date();
            SimpleDateFormat formatador = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            // Gets the data repository in write mode
            SQLiteDatabase db;
            db = mDbHelper.getWritableDatabase();

            String dataString = formatador.format(data);

            if (!exiteHumorporData(db, dataString, posicao)){
                // Create a new map of values, where column names are the keys
                ContentValues values = new ContentValues();
                values.put(FeedReaderContract.tabelaHumor.COLUMN_DATA, dataString);
                values.put(FeedReaderContract.tabelaHumor.COLUMN_NIVEL, posicao);
                try {
                    //if (verificarseJaTemAChave(db, cupom.getChaveDeAcesso())) {
                    db.insert(FeedReaderContract.tabelaHumor.TABLE_NAME, null, values);
                    //} else {
                    //throw new Exception("Cupom já salvo!");
                    //}
                    Log.i("Salvar", "Salvo com sucesso");
                } catch (Exception e){
                    return e;
                }
            }

            return null;
        }

        private boolean exiteHumorporData(SQLiteDatabase db, String data, int nivel){
            List<Integer> nivelList = new ArrayList<>();
            int id;
            Cursor cursor = db.rawQuery("select * from HUMOR where data='"+data+"'", null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                do {
                    //Pega cupom string do banco
                    id = cursor.getInt(
                            cursor.getColumnIndexOrThrow(FeedReaderContract.tabelaHumor._ID));
                    int nivel1 = cursor.getInt(
                            cursor.getColumnIndexOrThrow(FeedReaderContract
                                    .tabelaHumor.COLUMN_NIVEL)
                    );

                    // Adiciona cupom a lista
                    nivelList.add(nivel1);
                } while (cursor.moveToNext());
                // Retorna a media dos niveis
                mediaNivel(db, id, nivel, nivelList);
                cursor.close();
                return true;
            } else {
                // Se não existe nenhum humor na data, então o nível será o nível passado
                return false;
                //return nivel;
            }
        }

        private void mediaNivel(SQLiteDatabase db, int id,int nivel, List<Integer>niveis){
            int somaniveis = 0;
            int numeromedia = niveis.size() + 1;
            for (int i = 0; i < niveis.size(); i++) {
                somaniveis += niveis.get(i);
            }
            somaniveis +=nivel;
            int media = Math.round((float) somaniveis/numeromedia);

            updateHumorExistente(db, id, media);
        }

        private void updateHumorExistente(SQLiteDatabase db, int id, int media){
            db.execSQL("update HUMOR set nivel="+media+" where _id="+id);
            Log.i("Salvar", "Atualizado com sucesso!");
        }

        @Override
        protected void onPostExecute(Exception e) {
            super.onPostExecute(e);
        }
    }

    private class MyAdapter extends BaseAdapter {
        private int[] mResourceIds = {R.mipmap.img1, R.mipmap.img2, R.mipmap.img3, R.mipmap.img4,
                R.mipmap.img5, R.mipmap.img6, R.mipmap.img7};

        private int mCount = mResourceIds.length;// * 5;

        @Override
        public int getCount() {
            return mCount;
        }

        @Override
        public Object getItem(int position) {
            return mResourceIds[position % mResourceIds.length];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            MyFrame v;
            if (convertView == null) {
                v = new MyFrame(getContext());
            } else {
                v = (MyFrame)convertView;
            }

            v.setImageResource(mResourceIds[position % mResourceIds.length]);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(getContext(), "Clicou no "+position, Toast.LENGTH_SHORT).show();
                }
            });

            return v;
        }
    }

    public static class MyFrame extends FrameLayout {
        private ImageView mImageView;

        public void setImageResource(int resId){
            mImageView.setImageResource(resId);
        }

        public MyFrame(Context context) {
            super(context);

            mImageView = new ImageView(context);
            mImageView.setScaleType(ImageView.ScaleType.FIT_XY);
            addView(mImageView);

            //setBackgroundColor(Color.WHITE);
            setSelected(false);
        }

        @Override
        public void setSelected(boolean selected) {
            super.setSelected(selected);

            if(selected) {
                mImageView.setAlpha(1.0f);
            } else {
                mImageView.setAlpha(0.5f);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_meu_humor, container, false);
    }

}
