package com.alysson.teste.testedepre.ui.fragments;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.alysson.teste.testedepre.DB.FeedReaderContract;
import com.alysson.teste.testedepre.DB.FeedReaderDbHelper;
import com.alysson.teste.testedepre.R;
import com.alysson.teste.testedepre.model.Depressao;
import com.alysson.teste.testedepre.model.Fobia_Social;
import com.alysson.teste.testedepre.model.Humor;
import com.alysson.teste.testedepre.model.Solidao;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.listener.LineChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.Chart;
import lecho.lib.hellocharts.view.ColumnChartView;
import lecho.lib.hellocharts.view.LineChartView;

/**
 * A simple {@link Fragment} subclass.
 */
public class GraficoFragment extends Fragment {


    //Para Linhas
    private TextView txtResultadoFobiaSocial;
    private TextView txtResultadoSolidao;
    private LineChartView chart_line;
    private LineChartData data_line;
    private int maxNumberOfLines = 4;
    private int numberOfPoints_line = 12;
    //////////////

    List<Depressao> depressaoList;
    List<Humor> humorList;
    float[][] randomNumbersTab_line = new float[maxNumberOfLines][numberOfPoints_line];

    private boolean hasAxes_line = true;
    private boolean hasAxesNames_line = true;
    private boolean hasLines_line = true;
    private boolean hasPoints_line = true;
    private ValueShape shape_line = ValueShape.CIRCLE;
    private boolean isFilled_line = false;
    private boolean hasLabels_line = false;
    private boolean isCubic_line = false;
    private boolean hasLabelForSelected_line = false;

    // PARA COLUNAS

    private ColumnChartView chart;
    private ColumnChartData data;
    private boolean hasAxes = true;
    private boolean hasAxesNames = true;
    private boolean hasLabels = false;
    private boolean hasLabelForSelected = false;
    FeedReaderDbHelper mDbHelper;


    public GraficoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtResultadoFobiaSocial = (TextView) view.findViewById(R.id.txtResultadoFobiaSocial);
        txtResultadoSolidao = (TextView) view.findViewById(R.id.txtResultadoSolidao);
        chart_line = (LineChartView) view.findViewById(R.id.chart);
        chart_line.setOnValueTouchListener(new ValueTouchListener());
        chart = (ColumnChartView) view.findViewById(R.id.chart2);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Para ativar o menu do topo
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_grafico, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //LineChartView chart_line = new LineChartView(getContext());
        //chart_line
        mDbHelper = FeedReaderDbHelper.getInstance(getContext());
        new BuscarDepressao().execute();
        new BuscarHumor().execute();
        new BuscarFobiaSocial().execute();
        new BuscarSolidao().execute();
    }

    private void generateValues_line() {
        for (int i = 0; i < maxNumberOfLines; ++i) {
            for (int j = 0; j < numberOfPoints_line; ++j) {
                randomNumbersTab_line[i][j] = (float) Math.random() * 100f;
            }
        }
    }

    private void resetViewport_line() {
        // Reset viewport height range to (0,0)
        final Viewport v = new Viewport(chart_line.getMaximumViewport());
        v.bottom = 0;
        // 80 - É o máximo no teste de depressão
        v.top = 80;
        v.left = 0;
        v.right = numberOfPoints_line - 1;
        chart_line.setMaximumViewport(v);
        chart_line.setCurrentViewport(v);
    }

    private void generateData_line() {
        hasLabels_line = !hasLabels_line;

        if (hasLabels_line) {
            hasLabelForSelected_line = false;
            chart_line.setValueSelectionEnabled(false);
        }

        List<Line> lines = new ArrayList<>();
        for (int i = 0; i < depressaoList.size(); ++i) {

            List<PointValue> values = new ArrayList<>();
            for (int j = 0; j < depressaoList.size(); ++j) {
                //values.add(new PointValue(j, randomNumbersTab_line[i][j]));
                // Se aqui eu adicionar todos valores 0,
                // Quando fazer animação eu coloco os valores verdadeiros
                values.add(new PointValue(j, 0));
            }

            Line line = new Line(values);
            line.setColor(ChartUtils.COLORS[2]);
            line.setShape(shape_line);
            line.setCubic(isCubic_line);
            line.setFilled(isFilled_line);
            line.setHasLabels(hasLabels_line);
            line.setHasLabelsOnlyForSelected(hasLabelForSelected_line);
            line.setHasLines(hasLines_line);
            line.setHasPoints(hasPoints_line);
            lines.add(line);
        }

        data_line = new LineChartData(lines);

        if (hasAxes_line) {
            //Axis axisX = new Axis();
            Axis axisY = new Axis().setHasLines(true);
            if (hasAxesNames_line) {
                //axisX.setName("Axis X");
                axisY.setName("Axis Y");
            }
            //data_line.setAxisXBottom(axisX);
            data_line.setAxisYLeft(axisY);
        } else {
            //data_line.setAxisXBottom(null);
            data_line.setAxisYLeft(null);
        }

        data_line.setBaseValue(Float.NEGATIVE_INFINITY);
        chart_line.setLineChartData(data_line);

        prepareDataAnimation_line();
        chart_line.startDataAnimation();

    }

    private class BuscarDepressao extends AsyncTask<Void, Void, List<Depressao>>{

        @Override
        protected List<Depressao> doInBackground(Void... params) {
            SQLiteDatabase db = mDbHelper.getReadableDatabase();
            List<Depressao> depressaoList = new ArrayList<>();

            Cursor cursor = db.rawQuery("select * from DEPRESSAO ORDER BY date(DATA)", null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                do {
                    //Pega cupom string do banco
                    int _id = cursor.getInt(
                            cursor.getColumnIndexOrThrow(FeedReaderContract.tabelaDepressao._ID));
                    int pontos = cursor.getInt(
                            cursor.getColumnIndexOrThrow(FeedReaderContract
                                    .tabelaDepressao.COLUMN_PONTOS)
                    );
                    String data = cursor.getString(
                            cursor.getColumnIndexOrThrow(FeedReaderContract
                                    .tabelaDepressao.COLUMN_DATA)
                    );
                    depressaoList.add(new Depressao(_id, pontos, data));
                    // Adiciona cupom a lista
                    //listacupons.add(converterStringparaJson(cupomString));
                } while (cursor.moveToNext());
            }
            cursor.close();

            return depressaoList;
        }

        @Override
        protected void onPostExecute(List<Depressao> depressaos) {
            super.onPostExecute(depressaos);
            depressaoList = depressaos;

            // Generate some random values.
            generateValues_line();
            generateData_line();

            chart_line.setInteractive(true);
            // Disable viewport recalculations, see toggleCubic() method for more info.
            chart_line.setViewportCalculationEnabled(false);

            resetViewport_line();
        }
    }

    private class BuscarHumor extends AsyncTask<Void, Void, List<Humor>>{

        @Override
        protected List<Humor> doInBackground(Void... params) {
            SQLiteDatabase db = mDbHelper.getReadableDatabase();
            List<Humor> humorList = new ArrayList<>();

            Cursor cursor = db.rawQuery("select * from HUMOR ORDER BY date(DATA)", null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                do {
                    //Pega cupom string do banco
                    int _id = cursor.getInt(
                            cursor.getColumnIndexOrThrow(FeedReaderContract.tabelaHumor._ID));
                    int nivel = cursor.getInt(
                            cursor.getColumnIndexOrThrow(FeedReaderContract
                                    .tabelaHumor.COLUMN_NIVEL)
                    );
                    String data = cursor.getString(
                            cursor.getColumnIndexOrThrow(FeedReaderContract
                                    .tabelaDepressao.COLUMN_DATA)
                    );
                    humorList.add(new Humor(_id, nivel, data));
                    // Adiciona cupom a lista
                    //listacupons.add(converterStringparaJson(cupomString));
                } while (cursor.moveToNext());
            }
            cursor.close();

            return humorList;
        }

        @Override
        protected void onPostExecute(List<Humor> humors) {
            super.onPostExecute(humors);
            humorList = humors;

            generateData();

            chart.setInteractive(true);
            // Disable viewport recalculations, see toggleCubic() method for more info.
            chart.setViewportCalculationEnabled(false);

            resetViewport();
        }
    }

    private class BuscarFobiaSocial extends AsyncTask<Void, Void, Fobia_Social>{

        @Override
        protected Fobia_Social doInBackground(Void... params) {
            SQLiteDatabase db = mDbHelper.getReadableDatabase();
            Fobia_Social fobia_social = null;

            Cursor cursor = db.rawQuery("select * from FOBIA_SOCIAL", null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                do {
                    //Pega cupom string do banco
                    int _id = cursor.getInt(
                            cursor.getColumnIndexOrThrow(FeedReaderContract.tabelaFobiaSocial._ID));
                    int pontos = cursor.getInt(
                            cursor.getColumnIndexOrThrow(FeedReaderContract
                                    .tabelaFobiaSocial.COLUMN_PONTOS)
                    );
                    String data = cursor.getString(
                            cursor.getColumnIndexOrThrow(FeedReaderContract
                                    .tabelaFobiaSocial.COLUMN_DATA)
                    );
                   fobia_social = new Fobia_Social(_id, pontos, data);
                    // Adiciona cupom a lista
                    //listacupons.add(converterStringparaJson(cupomString));
                } while (cursor.moveToNext());
            }
            cursor.close();

            return fobia_social;
        }

        @Override
        protected void onPostExecute(Fobia_Social fobia_social) {
            super.onPostExecute(fobia_social);
            if (fobia_social!=null){
                if (fobia_social.getPontos() > 95) {
                    txtResultadoFobiaSocial.setText(R.string.fobsocial_muitograve);
                } else if (fobia_social.getPontos() >= 81) {
                    txtResultadoFobiaSocial.setText(R.string.fobsocial_grave);
                } else if (fobia_social.getPontos() >= 66 ) {
                    txtResultadoFobiaSocial.setText(R.string.fobsocial_media);
                } else if (fobia_social.getPontos() >= 55) {
                    txtResultadoFobiaSocial.setText(R.string.fobsocial_moderada);
                } else if (fobia_social.getPontos() < 55){
                    txtResultadoFobiaSocial.setText(R.string.fobsocial_nenhum);
                }
            }
        }
    }

    private class BuscarSolidao extends AsyncTask<Void, Void, Solidao>{

        @Override
        protected Solidao doInBackground(Void... params) {
            SQLiteDatabase db = mDbHelper.getReadableDatabase();
            Solidao solidao = null;
//
            Cursor cursor = db.rawQuery("select * from SOLIDAO", null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                do {
                    //Pega cupom string do banco
                    int _id = cursor.getInt(
                            cursor.getColumnIndexOrThrow(FeedReaderContract.tabelaFobiaSocial._ID));
                    int pontos = cursor.getInt(
                            cursor.getColumnIndexOrThrow(FeedReaderContract
                                    .tabelaSolidao.COLUMN_PONTOS)
                    );
                    String data = cursor.getString(
                            cursor.getColumnIndexOrThrow(FeedReaderContract
                                    .tabelaSolidao.COLUMN_DATA)
                    );
                    solidao = new Solidao(_id, pontos, data);
                    // Adiciona cupom a lista
                    //listacupons.add(converterStringparaJson(cupomString));
                } while (cursor.moveToNext());
            }
            cursor.close();

            return solidao;
        }

        @Override
        protected void onPostExecute(Solidao solidao) {
            super.onPostExecute(solidao);
            if (solidao!=null){
                if (solidao.getPontos() >= 30) {
                    txtResultadoSolidao.setText(R.string.solid_media3);
                } else if (solidao.getPontos() >= 25) {
                    txtResultadoSolidao.setText(R.string.solid_media2);
                } else if (solidao.getPontos() < 25 ) {
                    txtResultadoSolidao.setText(R.string.solid_media1);
                }
            }
        }
    }

    /**
     * To animate values you have to change targets values and then call {@link Chart#startDataAnimation()}
     * method(don't confuse with View.animate()). If you operate on data_line that was set before you don't have to call
     * {@link LineChartView#setLineChartData(LineChartData)} again.
     */
    private void prepareDataAnimation_line() {
        for (Line line : data_line.getLines()) {
            for (int i = 0; i < line.getValues().size(); i++) {
                line.getValues().get(i).setTarget(line.getValues().get(i).getX(), depressaoList.get(i).getPontos());
            }
//            for (PointValue value : line.getValues()) {
//                // Here I modify target only for Y values but it is OK to modify X targets as well.
//                value.setTarget(value.getX(), (float) Math.random() * 100);
//            }
        }
    }

    private class ValueTouchListener implements LineChartOnValueSelectListener {

        @Override
        public void onValueSelected(int lineIndex, int pointIndex, PointValue value) {
            Toast.makeText(getActivity(), "Selected: " + value, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onValueDeselected() {
            // TODO Auto-generated method stub

        }

    }


    // PARA COLUNAS
    private void generateDefaultData() {
        int numSubcolumns = 1;
        int numColumns = humorList.size();
        // Column can have many subcolumns, here by default I use 1 subcolumn in each of 8 columns.
        List<Column> columns = new ArrayList<>();
        List<SubcolumnValue> values;
        for (int i = 0; i < numColumns; ++i) {

            values = new ArrayList<>();
            for (int j = 0; j < numSubcolumns; ++j) {
                //values.add(new SubcolumnValue((float) Math.random() * 50f + 5, ChartUtils.pickColor()));
                values.add(new SubcolumnValue(0, ChartUtils.pickColor()));
            }

            Column column = new Column(values);
            column.setHasLabels(hasLabels);
            column.setHasLabelsOnlyForSelected(hasLabelForSelected);
            columns.add(column);
        }

        data = new ColumnChartData(columns);

        if (hasAxes) {
            //Axis axisX = new Axis();
            Axis axisY = new Axis().setHasLines(true);
            if (hasAxesNames) {
                //axisX.setName("Axis X");
                axisY.setName("Axis Y");
            }
            //data.setAxisXBottom(axisX);
            data.setAxisYLeft(axisY);
        } else {
            //data.setAxisXBottom(null);
            data.setAxisYLeft(null);
        }

        chart.setColumnChartData(data);

    }

    private void prepareDataAnimation() {
        int contador = 0;
        for (Column column : data.getColumns()) {
            for (SubcolumnValue value : column.getValues()) {
                //value.setTarget((float) Math.random() * 100);
                value.setTarget((float)humorList.get(contador).getNivel());
                contador++;
            }

        }
    }

    private void resetViewport() {
        // Reset viewport height range to (0,0)
        final Viewport v = new Viewport(chart.getMaximumViewport());
        // 80 - É o máximo no teste de depressão
        v.top = 7;
        chart.setMaximumViewport(v);
        chart.setCurrentViewport(v);
    }

    private void generateData() {
        hasLabels = !hasLabels;

        if (hasLabels) {
            hasLabelForSelected = false;
            chart.setValueSelectionEnabled(false);
        }

        generateDefaultData();

        prepareDataAnimation();
        chart.startDataAnimation();
    }

}
