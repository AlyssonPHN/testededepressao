package com.alysson.teste.testedepre.DB;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created para criar, atualiar ou downgrar as tabelas do banco, como o banco em si
 *
 */

public class FeedReaderDbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    private static FeedReaderDbHelper ourInstance;
    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "Marshall_testedepre.db";

    public static FeedReaderDbHelper getInstance(Context context) {
        if (ourInstance == null){
            ourInstance = new FeedReaderDbHelper(context);
        }
        return ourInstance;
    }

    private FeedReaderDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(FeedReaderContract.SQL_CREATE_TABELA_DEPRESSAO);
        db.execSQL(FeedReaderContract.SQL_CREATE_TABELA_HUMOR);
        db.execSQL(FeedReaderContract.SQL_CREATE_TABELA_FOBIA_SOCIAL);
        db.execSQL(FeedReaderContract.SQL_CREATE_TABELA_SOLIDAO);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Aqui ficará todos os updrade no Banco
        db.execSQL(FeedReaderContract.SQL_CREATE_TABELA_FOBIA_SOCIAL);
        db.execSQL(FeedReaderContract.SQL_CREATE_TABELA_SOLIDAO);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
