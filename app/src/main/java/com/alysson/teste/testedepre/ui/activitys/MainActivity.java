package com.alysson.teste.testedepre.ui.activitys;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.alysson.teste.testedepre.ui.fragments.SinaisFragment;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.alysson.teste.testedepre.R;
import com.alysson.teste.testedepre.ui.fragments.fragmentosFobiaSocial.HomeFSocialFragment;
import com.alysson.teste.testedepre.ui.fragments.GraficoFragment;
import com.alysson.teste.testedepre.ui.fragments.MeuHumorFragment;
import com.alysson.teste.testedepre.ui.fragments.fragmentsPerguntas.HomeFragment;
import com.alysson.teste.testedepre.ui.fragments.fragmentsSolidao.HomeSolidaoFragment;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.appindexing.Action;
import com.google.firebase.appindexing.FirebaseUserActions;
import com.google.firebase.appindexing.builders.Actions;
import com.tjeannin.apprate.AppRate;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public NavigationView navigationView;
    FrameLayout container_body;
    //private Fragment fragmentAtual;
    @SuppressLint("StaticFieldLeak")
    public static Toolbar toolbar;
    public DrawerLayout drawer;
    private AdView adView;
    public static InterstitialAd interstitialAd;
    public static int contador;
    public static int contadorFobiaSocial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Button btnAds = (Button) findViewById(R.id.btnads);
        adView = findViewById(R.id.adView);
        container_body = findViewById(R.id.container_body);
        setSupportActionBar(toolbar);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);


        // Parte do Drawer Navigation
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        //setDrawerIndicatorEnabled
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Propaganda
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        //Mostra a propagando (Banner) na parte inferior do app
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.banner_intersticial_unit_id));

        //Faz a requisição do propaganda intersticial
        loadIntersticial();
        //Para fazer novamente a requisição no caso de ser fechada
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                loadIntersticial();
                super.onAdClosed();
            }
        });
        //Mostra a requisição através do click
//        btnAds.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                interstitialAd.show();
//            }
//        });

        mostrarFragment(new GraficoFragment());
        avaliar();
    }

    private void avaliar(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                //.setCustomTitle(myCustomTitleView)
                //
                //.setIcon(R.drawable.my_custom_icon)
                .setMessage(R.string.avaliarnos)
                .setPositiveButton(R.string.agora, null)
                .setNeutralButton(R.string.depois, null)
                .setNegativeButton(R.string.nunca, null);


        new AppRate(this)
                .setCustomDialog(builder)
                .setMinDaysUntilPrompt(0)
                .setMinLaunchesUntilPrompt(2)
                .init();
    }


    @Override
    protected void onPause() {
        if (adView != null) {
            adView.pause();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    protected void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        //Para indexar aplicação na pesquisa Google
////        Indexable note = Indexables.noteDigitalDocumentBuilder()
////                .setUrl("https://play.google.com/store/apps/details?id=com.alysson.teste.testedepre")
////                .setName("Teste de Depressão")
////                .setText("Teste, Depressão, Zung")
////                .setImage("https://lh3.googleusercontent.com/o8ux76oqnrV6qwAlEpyyhrsmy2EY8-VZbl8_uTUrH8XdI7lb6-QKRLglqx1TTf6Pdg=w300-rw")
////                .build();
////        FirebaseAppIndex.getInstance().update(note);
////        FirebaseUserActions.getInstance().start(getAction());
//    }

    @Override
    protected void onStop() {
        //Para indexação da aplicação
        FirebaseUserActions.getInstance().end(getAction());
        super.onStop();
    }

    private Action getAction() {
        return Actions.newView("Teste de Depressão", "https://play.google.com/store/apps/details?id=com.alysson.teste.testedepre");
    }

    private void loadIntersticial() {
        //Faz a requisição da propaganda intersticial (Tela completa)
        AdRequest interAdRequest = new AdRequest.Builder().build();
        interstitialAd.loadAd(interAdRequest);
    }

    public static int getContador() {
        return contador;
    }

    public static void setContador(int valor) {
        contador = contador + valor;
    }

    public static int getContadorFobiaSocial() {
        return contadorFobiaSocial;
    }

    public static void setContadorFobiaSocial(int valor) {
        contadorFobiaSocial = contadorFobiaSocial + valor;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.grafico:
                fragment = new GraficoFragment();
                break;
            case R.id.teste_depressao:
                fragment = new HomeFragment();
                break;
            case R.id.teste_fobiasocial:
                fragment = new HomeFSocialFragment();
                break;
            case R.id.sinais_externos:
                fragment = SinaisFragment.newInstance(0);
                break;
            case R.id.sinais_internos:
                fragment = SinaisFragment.newInstance(1);
                break;
            case R.id.teste_solidao:
                fragment = new HomeSolidaoFragment();
                break;
            case R.id.meu_humor:
                fragment = new MeuHumorFragment();
                break;
//            case R.id.meu_chat:
//                //val i = Intent(this, ActEdicao::class.java)
//                Intent intent = new Intent(this, com.alysson.teste.testedepre.chat.MainActivity.class);
//                startActivity(intent);
//                break;

        }

        //Esconde menu lateral quando clicado
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        //Define o fragmento a ser executado
        if (fragment != null) {
            mostrarFragment(fragment);
        }
        return true;
    }
    private void mostrarFragment(Fragment fragment) {
        //Limpar contador ao mudar de fragment
        contador = 0;
        contadorFobiaSocial = 0;
        //fragmentAtual = fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
    }
}
