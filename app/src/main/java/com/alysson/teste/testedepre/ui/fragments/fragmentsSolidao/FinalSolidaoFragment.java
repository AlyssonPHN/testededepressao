package com.alysson.teste.testedepre.ui.fragments.fragmentsSolidao;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.alysson.teste.testedepre.DB.FeedReaderContract;
import com.alysson.teste.testedepre.DB.FeedReaderDbHelper;
import com.alysson.teste.testedepre.R;
import com.alysson.teste.testedepre.ui.activitys.MainActivity;
import com.alysson.teste.testedepre.controle.MetodosemComuns;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class FinalSolidaoFragment extends Fragment implements View.OnClickListener {

    private TextView respostaTitle, resposta, txtpontos;
    private Button btn_proximo;

    public FinalSolidaoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_final_solidao, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        respostaTitle = (TextView) view.findViewById(R.id.respostaTitle);
        resposta = (TextView) view.findViewById(R.id.resposta);
        txtpontos = (TextView) view.findViewById(R.id.pontos);
        btn_proximo = (Button) view.findViewById(R.id.btn_proximo);
        btn_proximo.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //Mostra a propaganda intersticial ao entrar na tela final
        MainActivity.interstitialAd.show();

        //Calcular Resposta
        mostrarResposta();
        new SalvarSolidao().execute();
    }

    private void mostrarResposta() {
        if (MainActivity.getContador() >= 30) {
            respostaTitle.setText(R.string.solid_media3);
            resposta.setText(R.string.solid_result3);
        } else if (MainActivity.getContador() >= 25) {
            respostaTitle.setText(R.string.solid_media2);
            resposta.setText(R.string.solid_result2);
        } else if (MainActivity.getContador() < 25 ) {
            respostaTitle.setText(R.string.solid_media1);
            resposta.setText(R.string.solid_result1);
        }
    }

    private class SalvarSolidao extends AsyncTask<Void, Void, Exception> {

        // Acessar banco
        FeedReaderDbHelper mDbHelper = FeedReaderDbHelper.getInstance(getContext());

        @Override
        protected Exception doInBackground(Void... params) {
            Date data = new Date();
            SimpleDateFormat formatador = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            // Gets the data repository in write mode
            SQLiteDatabase db;
            db = mDbHelper.getWritableDatabase();

            if (exiteSolidaoporData(db)){
                //Se existe solidão, então limpar tabela
                db.execSQL("DELETE FROM SOLIDAO");
            }

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(FeedReaderContract.tabelaSolidao.COLUMN_PONTOS, MainActivity.getContador());
            values.put(FeedReaderContract.tabelaSolidao.COLUMN_DATA, formatador.format(data));

            try {
                db.insert(FeedReaderContract.tabelaSolidao.TABLE_NAME, null, values);
            } catch (Exception e) {
                return e;
            }

            return null;
        }

        private boolean exiteSolidaoporData(SQLiteDatabase db){
            int quantidade = 0;
            Cursor cursor = db.rawQuery("select COUNT(*) from SOLIDAO", null);

            if (cursor.moveToFirst()) {
                quantidade = cursor.getInt(0);
            }
            cursor.close();
            return quantidade > 0;
        }

        @Override
        protected void onPostExecute(Exception e) {
            super.onPostExecute(e);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btn_proximo) {
            //Mudar de fragment
            MainActivity.contador = 0;
            //Ir para o fragment inicial
            Fragment fragment = new HomeSolidaoFragment();
            MetodosemComuns.chamarFragment(getActivity(), fragment);
        }
    }
}
