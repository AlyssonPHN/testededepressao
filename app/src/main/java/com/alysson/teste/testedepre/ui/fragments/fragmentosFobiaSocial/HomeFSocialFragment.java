package com.alysson.teste.testedepre.ui.fragments.fragmentosFobiaSocial;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.alysson.teste.testedepre.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFSocialFragment extends Fragment implements View.OnClickListener {


    public HomeFSocialFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btn_proximo = (Button) view.findViewById(R.id.btn_proximo);
        btn_proximo.setOnClickListener(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_fsocial, container, false);
    }

    private Button btn_proximo;


    @Override
    public void onClick(View view) {
        if (view == btn_proximo){
            Fragment fragment = new Pergunta1FBSFragment();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }

    }

}
