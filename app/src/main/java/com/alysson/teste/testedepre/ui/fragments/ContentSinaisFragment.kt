package com.alysson.teste.testedepre.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.alysson.teste.testedepre.R
import com.alysson.teste.testedepre.ui.activitys.ActEdicao

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


/**
 * A simple [Fragment] subclass.
 * Use the [ContentSinaisFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ContentSinaisFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: Int? = null
    private var opcao: Int? = null

    private lateinit var txttitulo: TextView
    private lateinit var txtbody: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getInt(ARG_PARAM1)
            opcao = it.getInt(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_content_sinais, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txttitulo = view.findViewById(R.id.txttitulo)
        txtbody = view.findViewById(R.id.txtbody)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        preencherInformacoes()
        ActEdicao.fragment = null
    }

    private fun preencherInformacoes(){
        when(param1) {
            0 -> {
                when(opcao){
                    CORPO -> {
                        txttitulo.text = getString(R.string.titleCorpo)
                        txtbody.text = getString(R.string.txtcorpo_externo)
                    }
                    MENTE -> {
                        txttitulo.text = getString(R.string.titleMente)
                        txtbody.text = getString(R.string.txtmente_externa)
                    }
                    SOCIAL -> {
                        txttitulo.text = getString(R.string.titleSocial)
                        txtbody.text = getString(R.string.txtsocial_externos)
                    }
                    ESPIRITO -> {
                        txttitulo.text = getString(R.string.titleEspiritual)
                        txtbody.text = getString(R.string.txtespiritual_externo)
                    }
                }
            }
            1 -> {
                when(opcao){
                    CORPO -> {
                        txttitulo.text = getString(R.string.titleCorpo)
                        txtbody.text = getString(R.string.txtcorpo_interno)
                    }
                    MENTE -> {
                        txttitulo.text = getString(R.string.titleMente)
                        txtbody.text = getString(R.string.txtmente_interno)
                    }
                    SOCIAL -> {
                        txttitulo.text = getString(R.string.titleSocial)
                        txtbody.text = getString(R.string.txtsocial_interno)
                    }
                    ESPIRITO -> {
                        txttitulo.text = getString(R.string.titleEspiritual)
                        txtbody.text = getString(R.string.txtespiritual_interno)
                    }
                }
            }
        }
    }

    companion object {
        const val CORPO = 0
        const val MENTE = 1
        const val SOCIAL = 2
        const val ESPIRITO = 3

        @JvmStatic
        fun newInstance(param1: Int, opcao: Int) =
                ContentSinaisFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_PARAM1, param1)
                        putInt(ARG_PARAM2, opcao)
                    }
                }
    }
}