package com.alysson.teste.testedepre.model;

/**
 * Created by Bangu1 on 09/06/2017.
 */

public class Solidao {

    private int _id;
    private int pontos;
    private String data;

    public Solidao(int _id, int pontos, String data) {
        this._id = _id;
        this.pontos = pontos;
        this.data = data;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
