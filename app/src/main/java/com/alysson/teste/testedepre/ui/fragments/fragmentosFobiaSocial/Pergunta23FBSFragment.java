package com.alysson.teste.testedepre.ui.fragments.fragmentosFobiaSocial;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

import com.alysson.teste.testedepre.R;
import com.alysson.teste.testedepre.ui.activitys.MainActivity;
import com.alysson.teste.testedepre.controle.MetodosemComuns;

/**
 * A simple {@link Fragment} subclass.
 */
public class Pergunta23FBSFragment extends Fragment implements View.OnClickListener {//OnItemSelectedListener

    private Button btn_proximo;
    private Spinner spinnerAnsiedade, spinnerEvitamento;

    public Pergunta23FBSFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btn_proximo = (Button) view.findViewById(R.id.btn_proximo);
        spinnerAnsiedade = (Spinner) view.findViewById(R.id.spinner_ansiedade);
        spinnerEvitamento = (Spinner) view.findViewById(R.id.spinner_evitamento);
        btn_proximo.setOnClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pergunta23_fb, container, false);
    }


    @Override
    public void onClick(View v) {
        if (v == btn_proximo) {
            // Aplica valores para soma
            MainActivity.setContadorFobiaSocial(spinnerAnsiedade.getSelectedItemPosition());
            MainActivity.setContadorFobiaSocial(spinnerEvitamento.getSelectedItemPosition());

            //Mudar de fragment
            Fragment fragment = new Pergunta24FBSFragment();
            MetodosemComuns.chamarFragment(getActivity(), fragment);
        }
    }
}
