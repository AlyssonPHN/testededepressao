package com.alysson.teste.testedepre.ui.fragments.fragmentosFobiaSocial;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.alysson.teste.testedepre.DB.FeedReaderContract;
import com.alysson.teste.testedepre.DB.FeedReaderDbHelper;
import com.alysson.teste.testedepre.R;
import com.alysson.teste.testedepre.ui.activitys.MainActivity;
import com.alysson.teste.testedepre.controle.MetodosemComuns;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class FinalFBSFragment extends Fragment implements View.OnClickListener {

    private TextView resposta, txtpontos;
    private Button btn_proximo;

    public FinalFBSFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_final_fb, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        resposta = (TextView) view.findViewById(R.id.resposta);
        txtpontos = (TextView) view.findViewById(R.id.pontos);
        btn_proximo = (Button) view.findViewById(R.id.btn_proximo);
        btn_proximo.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //Mostra a propaganda intersticial ao entrar na tela final
        MainActivity.interstitialAd.show();

        //Calcular Resposta
        mostrarResposta();
        new SalvarFobiaSocial().execute();
    }

    private void mostrarResposta() {
        if (MainActivity.getContadorFobiaSocial() > 95) {
            resposta.setText(R.string.fobsocial_muitograve);
        } else if (MainActivity.getContadorFobiaSocial() >= 81) {
            resposta.setText(R.string.fobsocial_grave);
        } else if (MainActivity.getContadorFobiaSocial() >= 66 ) {
            resposta.setText(R.string.fobsocial_media);
        } else if (MainActivity.getContadorFobiaSocial() >= 55) {
            resposta.setText(R.string.fobsocial_moderada);
        } else if (MainActivity.contadorFobiaSocial < 55){
            resposta.setText(R.string.fobsocial_nenhum);
        }
        txtpontos.setText(String.valueOf(MainActivity.getContadorFobiaSocial()));
    }

    private class SalvarFobiaSocial extends AsyncTask<Void, Void, Exception> {

        // Acessar banco
        FeedReaderDbHelper mDbHelper = FeedReaderDbHelper.getInstance(getContext());

        @Override
        protected Exception doInBackground(Void... params) {
            Date data = new Date();
            SimpleDateFormat formatador = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            // Gets the data repository in write mode
            SQLiteDatabase db;
            db = mDbHelper.getWritableDatabase();

            if (exiteFobiaSocialporData(db)){
                //Se existe fobia social, então limpar tabela
                db.execSQL("DELETE FROM FOBIA_SOCIAL");
            }

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(FeedReaderContract.tabelaFobiaSocial.COLUMN_PONTOS, MainActivity.getContadorFobiaSocial());
            values.put(FeedReaderContract.tabelaFobiaSocial.COLUMN_DATA, formatador.format(data));

            try {
                db.insert(FeedReaderContract.tabelaFobiaSocial.TABLE_NAME, null, values);
            } catch (Exception e) {
                return e;
            }

            return null;
        }

        private boolean exiteFobiaSocialporData(SQLiteDatabase db){
            int quantidade = 0;
            Cursor cursor = db.rawQuery("select COUNT(*) from FOBIA_SOCIAL", null);

            if (cursor.moveToFirst()) {
                quantidade = cursor.getInt(0);
            }
            cursor.close();
            return quantidade > 0;
        }

        @Override
        protected void onPostExecute(Exception e) {
            super.onPostExecute(e);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btn_proximo) {
            //Mudar de fragment
            MainActivity.contadorFobiaSocial = 0;
            //Ir para o fragment inicial
            Fragment fragment = new HomeFSocialFragment();
            MetodosemComuns.chamarFragment(getActivity(), fragment);
        }
    }
}
