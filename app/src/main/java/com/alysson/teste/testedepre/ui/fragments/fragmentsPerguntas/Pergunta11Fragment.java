package com.alysson.teste.testedepre.ui.fragments.fragmentsPerguntas;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

import com.alysson.teste.testedepre.R;
import com.alysson.teste.testedepre.controle.MetodosemComuns;

/**
 * A simple {@link Fragment} subclass.
 */
public class Pergunta11Fragment extends Fragment implements View.OnClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pergunta11, container, false);
    }

    private Button btn_proximo;
    private RadioButton radioBtn1, radioBtn2, radioBtn3, radioBtn4;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btn_proximo = (Button) view.findViewById(R.id.btn_proximo);
        btn_proximo.setOnClickListener(this);
        radioBtn1 = (RadioButton) view.findViewById(R.id.radio_1);
        radioBtn2 = (RadioButton) view.findViewById(R.id.radio_2);
        radioBtn3 = (RadioButton) view.findViewById(R.id.radio_3);
        radioBtn4 = (RadioButton) view.findViewById(R.id.radio_4);
    }

    @Override
    public void onClick(View view) {
        if (view == btn_proximo){
            if (MetodosemComuns.verificarRadioButton(getActivity(), radioBtn1, radioBtn2, radioBtn3, radioBtn4)) {
                //Mudar de fragment
                Fragment fragment = new Pergunta12Fragment();
                MetodosemComuns.chamarFragment(getActivity(), fragment);
            }

        }
    }
}
