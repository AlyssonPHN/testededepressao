package com.alysson.teste.testedepre.DB;

import android.provider.BaseColumns;

/**
 * classe de acompanhamento, conhecida como classe de contrato, que especifica explicitamente
 * o detalhe_informacoes do esquema de forma sistemática e autodocumentada.
 */

public class FeedReaderContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private FeedReaderContract() {}

    /* Inner class that defines the table contents */
    public static class tabelaDepressao implements BaseColumns {
        public static final String TABLE_NAME = "DEPRESSAO";
        public static final String COLUMN_PONTOS = "PONTOS";
        public static final String COLUMN_DATA = "DATA";
    }

    public static class tabelaHumor implements BaseColumns {
        public static final String TABLE_NAME = "HUMOR";
        public static final String COLUMN_NIVEL = "NIVEL";
        public static final String COLUMN_DATA = "DATA";
    }

    public static class tabelaFobiaSocial implements BaseColumns{
        public static final String TABLE_NAME = "FOBIA_SOCIAL";
        public static final String COLUMN_PONTOS = "PONTOS";
        public static final String COLUMN_DATA = "DATA";
    }

    public static class tabelaSolidao implements BaseColumns{
        public static final String TABLE_NAME = "SOLIDAO";
        public static final String COLUMN_PONTOS = "PONTOS";
        public static final String COLUMN_DATA = "DATA";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    static final String SQL_CREATE_TABELA_DEPRESSAO =
            "CREATE TABLE " + tabelaDepressao.TABLE_NAME + " (" +
                    tabelaDepressao._ID + " INTEGER PRIMARY KEY," +
                    tabelaDepressao.COLUMN_PONTOS + INTEGER_TYPE + "," +
                    tabelaDepressao.COLUMN_DATA + TEXT_TYPE  + " )";

    static final String SQL_CREATE_TABELA_HUMOR =
            "CREATE TABLE " + tabelaHumor.TABLE_NAME + " (" +
                    tabelaHumor._ID + " INTEGER PRIMARY KEY," +
                    tabelaHumor.COLUMN_NIVEL + INTEGER_TYPE + "," +
                    tabelaHumor.COLUMN_DATA + TEXT_TYPE  + " )";

    static final String SQL_CREATE_TABELA_FOBIA_SOCIAL =
            "CREATE TABLE IF NOT EXISTS " + tabelaFobiaSocial.TABLE_NAME + " (" +
                    tabelaFobiaSocial._ID + " INTEGER PRIMARY KEY," +
                    tabelaFobiaSocial.COLUMN_PONTOS + INTEGER_TYPE + "," +
                    tabelaFobiaSocial.COLUMN_DATA + TEXT_TYPE  + " )";

    static final String SQL_CREATE_TABELA_SOLIDAO =
            "CREATE TABLE IF NOT EXISTS " + tabelaSolidao.TABLE_NAME + " (" +
                    tabelaSolidao._ID + " INTEGER PRIMARY KEY," +
                    tabelaSolidao.COLUMN_PONTOS + INTEGER_TYPE + "," +
                    tabelaSolidao.COLUMN_DATA + TEXT_TYPE  + " )";



}
