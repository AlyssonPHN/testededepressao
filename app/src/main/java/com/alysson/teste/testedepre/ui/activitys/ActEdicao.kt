package com.alysson.teste.testedepre.ui.activitys

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.alysson.teste.testedepre.R

class ActEdicao : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configuracao)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar!!.title = tituloFragmento
        //Essa linha é responsável por ativar o botão voltar
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        //Enviando variavel que é para ver se é novo cliente Edição Cliente
//        val bundle = Bundle()
//        bundle.putBoolean("addnovo", addnovo)
//        fragment!!.arguments = bundle

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.container_body, fragment!!)
        fragmentTransaction.commit()

    }

    override fun onBackPressed() {
        if (fragment != null) {
            //user defined onBackPressed method. Not of Fragment.
            fragment!!.onDetach()
        } else {
            //this will pass BackPress event to activity. If not called, it will
            //prevent activity to get BackPress event.
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        fragment = null
        tituloFragmento = null
    }

    companion object {

        var fragment: androidx.fragment.app.Fragment? = null
        var tituloFragmento: String? = null
        var addnovo: Boolean = false
    }
}
