package com.alysson.teste.testedepre.model;


public class Humor {

    private int _id;
    private int nivel;
    private String data;

    public Humor(int _id, int nivel, String data) {
        this._id = _id;
        this.nivel = nivel;
        this.data = data;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
